---
title: "Update on our planned move from Azure to Google Cloud Platform"
author: David Smith
author_gitlab: dawsmith
author_twitter: dsmith8641
categories: engineering
image_title: '/images/blogimages/gitlab-gke-integration-cover.png'
description: "GitLab.com is migrating to Google Cloud Platform July 28 – here’s what this means for you now and in the future."
tags: google, cloud native, GKE, kubernetes
ee_cta: false
twitter_text: "We're moving @gitlab from Azure to @GCPcloud"
---

Improving the performance and reliability of [GitLab.com](/pricing/#gitlab-com) has been a top priority for us. On this front we've made some incremental gains while we've been planning for a large change with the potential to net significant results: running GitLab as a [cloud native](/cloud-native) application on Kubernetes.

The next incremental step on our cloud native journey is a big one: migrating from Azure to Google Cloud Platform (GCP). While Azure has been a great provider for us, GCP has the best Kubernetes support and we believe will the best provider for our long-term plans. In the short term, our users will see some immediate benefits once we cut over from Azure to GCP including encrypted data at rest on by default and faster caching due to GCP's tight integration with our existing CDN.

## Upcoming maintenance windows for the GCP migration

As an update to [our earlier blog post on the migration](https://about.gitlab.com/2018/06/25/moving-to-gcp/), this is a short post to let our community know we are planning on performing the migration of GitLab.com the weekend of July 28. We have two maintenance windows coming up that we would like to make sure everybody knows about.

### What you need to know:

During the maintenance windows, the following services will be unavailable:

* SaaS website ([GitLab.com](https://gitlab.com/) will be offline, but [about.gitlab.com](https://about.gitlab.com/) and [docs.gitlab.com](https://docs.gitlab.com/) will still be available)
* Git ssh
* Git https
* registry
* CI/CD

### Saturday July 21 at 13:00 UTC

As a further update to our testing, we are planning to take a short maintenance window this weekend on Saturday, July 21 at 13:00 UTC to do final readiness checks.
This maintenance window should last one hour.

### Saturday July 28 at 10:00 UTC

On the day of the migration, we are planning to start at 10:00 UTC.  The time window for GitLab.com to be in maintenance is currently planned to be two hours.  Should any times for this change, we will be updating on the channels listed below. When this window is completed GitLab.com will be running out of GCP.

* [GitLab Status page](https://status.gitlab.com/)
* [GitLab Status Twitter](https://twitter.com/gitlabstatus)

### GitLab Pages and custom domains

If you have a custom domain on [GitLab Pages](https://about.gitlab.com/features/pages/):

* We will have a proxy in place so you do not have to change your DNS immediately.  
* GitLab Pages will ultimately go to 35.185.44.232 after the July 28 migration.  
* Do not change your DNS to this new address until we have successfully completed the migration.
* We will post an update to our blog about when the cutoff will be for changing DNS from our Azure address to GCP for GitLab Pages.

Should you need support during the migration, please reach out to [GitLab Support](https://about.gitlab.com/support/).

Wish us luck!
